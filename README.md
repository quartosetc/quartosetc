A Quartos Etc. é referência no mercado de decoração de luxo mantendo sua história e sempre à frente em busca de novidades e melhores maneiras de realizar os sonhos de nossos clientes! Acreditamos que o quarto é o lugar onde regeneramos o nosso corpo e energia, já que passamos aproximadamente metade de nossa vida nele. Estamos localizados na Alameda Gabriel Monteiro da Silva, maior polo de decoração de São Paulo.

Mais do que móveis a Quartos Etc. se preocupa em criar qualidade de vida, criando produtos que possam fazer parte da construção desse espaço tão especial!

Contato
QUARTOS ETC. - Móveis para Quartos Customizados
Al. Gabriel Monteiro da Silva, 1742 - Jardim Paulistano
CEP: 01441-000
LOJA (11) 3085-2800
CELULAR (11) 99369-9219
Email: contato@quartosetc.com.br
Website: https://quartosetc.com.br